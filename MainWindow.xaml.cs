﻿using SharpSvn;
using System;
using System.IO;
using System.Linq;
using System.Timers;
using System.Windows;
using System.Diagnostics;
using System.Windows.Input;
using System.ComponentModel;
using System.Collections.Generic;
using MahApps.Metro.Controls;
using System.Windows.Controls;
using System.Runtime.InteropServices;

namespace AMDB_Task_Monitor
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        private string swRevision = "2.0.0.5";
        bool prevPlayStatus = true;
        bool prevPauseStatus = false;
        private Process airportProcess = null;
        int currentTask = 0;
        private string savePath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
                                    "\\PSW_TaskMonitoring";
        private string svnLoc = "";

        JIRA JiraMan = new JIRA();
        Database db;
        List<Tasks> dispTaskList;
        List<Tasks> taskList = new List<Tasks>();

        System.Windows.Forms.NotifyIcon tBarIcon = new System.Windows.Forms.NotifyIcon();

        public MainWindow()
        {
            //Check if another instance is running
            Process[] pname = Process.GetProcessesByName(Process.GetCurrentProcess().ProcessName);
            if (pname.Length > 1)
            {
                MessageBox.Show("Another instance is already running.");
                Application.Current.Shutdown();
            }

            InitializeComponent();


            db = new Database(savePath + "\\DB.sqlite");

            //Initialize Notify Icon
            tBarIcon.Icon = AMDB_Task_Monitor.Properties.Resources.twrk;
            tBarIcon.DoubleClick += TBarIcon_DoubleClick;
            tBarIcon.BalloonTipClicked += TBarIcon_DoubleClick;
            tBarIcon.Visible = true;

            //Initialize Notify Icon
            tBarIcon.Icon = AMDB_Task_Monitor.Properties.Resources.twrk;

            //Disable Pause Button
            btnPause.IsEnabled = false;

            //Disable future dates
            selectedDate.DisplayDateEnd = DateTime.Now;

            //Set today's date
            selectedDate.SelectedDate = DateTime.Now;

            //Start AutoSave BackGround Worker
            BackGroundUpdate();            

            //Start MISC Task
            StartMiscTask();

            //Get or Set the SVN location for Aerodrome
            svnLoc = getSVNLocation();
        }

        //Events to handle on LogIn Screen
        #region LogIn Events
        private void btnLogIn_Click(object sender, RoutedEventArgs e)
        {
            LogIn(tBoxUname.Text, tBoxPword.Password, cBoxMarket.Text);

        }

        private void tBoxPword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                LogIn(tBoxUname.Text, tBoxPword.Password, cBoxMarket.Text);
            }
        }
        #endregion

        //Events to handle on Main Window
        #region Main Form Events
        private void TBarIcon_DoubleClick(object sender, EventArgs e)
        {
            //this.ShowInTaskbar = true;
            this.Show();
            WindowState = WindowState.Normal;
            this.Activate();
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            GetAssignedIssues();
        }

        private void btnPlay_Click(object sender, RoutedEventArgs e)
        {
            if (cboxTaskName.Text.Trim() != String.Empty)
            {
                try
                {
                    UpdateCurrentTask();                    
                    taskListTbl.Items.Refresh();

                    if (AddNewTask())
                    {
                        btnPlay.IsEnabled = false;
                        btnRefresh.IsEnabled = false;
                        btnPause.IsEnabled = true;

                        if (taskList[currentTask-1].Hours < taskList[currentTask - 1].Hours.Date.AddMinutes(5))
                        {
                            db.DeleteTask(
                                taskList[currentTask - 1].Name,
                                taskList[currentTask - 1].StartTime,
                                taskList[currentTask - 1].EndTime,
                                taskList[currentTask - 1].Hours);
                            taskListTbl.Items.RemoveAt(currentTask - 1);
                            taskList.RemoveAt(currentTask - 1);
                            currentTask--;
                        }
                    }
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                MessageBox.Show("Please Fill Current Task", "Empty Task", MessageBoxButton.OK, MessageBoxImage.Information);
            }

        }

        [DllImport("user32.dll")]
        static extern bool SetForegroundWindow(IntPtr hWnd);

        private void btnPause_Click(object sender, RoutedEventArgs e)
        {
            if(airportProcess != null && airportProcess.HasExited)
            {
                try
                {
                    airportProcess = null;
                    UpdateCurrentTask();
                    if (taskList[currentTask].Hours < taskList[currentTask].Hours.Date.AddMinutes(5))
                    {
                        db.DeleteTask(
                            taskList[currentTask].Name,
                            taskList[currentTask].StartTime,
                            taskList[currentTask].EndTime,
                            taskList[currentTask].Hours);
                        taskListTbl.Items.RemoveAt(currentTask);
                        taskList.RemoveAt(currentTask);
                    }
                    taskListTbl.Items.Refresh();
                    StartMiscTask();

                    btnPlay.IsEnabled = true;
                    btnRefresh.IsEnabled = true;
                    btnPause.IsEnabled = false;
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else if(airportProcess != null)
            {
                SetForegroundWindow(airportProcess.MainWindowHandle);
                airportProcess.CloseMainWindow();
            }
            else
            {
                UpdateCurrentTask();
                if (taskList[currentTask].Hours < taskList[currentTask].Hours.Date.AddMinutes(5))
                {
                    db.DeleteTask(
                        taskList[currentTask].Name,
                        taskList[currentTask].StartTime,
                        taskList[currentTask].EndTime,
                        taskList[currentTask].Hours);
                    taskListTbl.Items.RemoveAt(currentTask);
                    taskList.RemoveAt(currentTask);
                }
                taskListTbl.Items.Refresh();
                StartMiscTask();

                btnPlay.IsEnabled = true;
                btnRefresh.IsEnabled = true;
                btnPause.IsEnabled = false;
            }
        }

        private void Main_StateChanged(object sender, EventArgs e)
        {
            if (WindowState == WindowState.Minimized)
            {
                //this.ShowInTaskbar = false;
                this.Hide();
            }
        }

        private void Main_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            tBarIcon.Visible = false;
        }
        #endregion

        //Functions/Methods used by the program
        #region Function/Methods
                
        private string getSVNLocation()
        {
            string svnTxtLoc = savePath + "\\" + "svnLoc.txt";
            if (!File.Exists(svnTxtLoc))
            {
                StreamWriter file = File.CreateText(svnTxtLoc);
                System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();
                dialog.Description = "Aerodrome_AIB folder";
                System.Windows.Forms.DialogResult dr = System.Windows.Forms.DialogResult.Cancel;
                do
                {
                    dr = dialog.ShowDialog();
                } while (dr != System.Windows.Forms.DialogResult.OK);
                file.WriteLine(dialog.SelectedPath);
                file.Close();
                return dialog.SelectedPath;
            }
            else
            {
                string svnLoc = File.ReadAllLines(svnTxtLoc)[0];
                return svnLoc;
            }
        }
    
        private bool AddNewTask()
        {
            string taskName;
            if (cboxTaskName.Text.Contains("-"))
                taskName = cboxTaskName.Text.ToUpper().Remove(0, cboxTaskName.Text.IndexOf('-') + 2);
            else
                taskName = cboxTaskName.Text.ToUpper();

            string supFolder = svnLoc + @"\databases\SUP\" + taskName;
            string vecFolder = svnLoc + @"\databases\VEC\" + taskName;
            string toolFolder = svnLoc + @"\tools";

            string airportExe = svnLoc + @"\tools\Airport.exe";            
            string wrkLoc = svnLoc + @"\databases\VEC\" + taskName +
                "\\" + taskName + ".wrk";
            string vecLoc = svnLoc + @"\databases\VEC\" + taskName +
                "\\" + taskName + ".vec";

            if(!File.Exists(vecLoc))
            {
                dispTaskList = new List<Tasks>(taskList);

                currentTask++;
                taskList.Add(new Tasks());
                taskListTbl.Items.Add(taskList[currentTask]);
                taskList[currentTask].Name = taskName;
                taskList[currentTask].StartTime = DateTime.Now;
                taskListTbl.Items.Refresh();

                db.CreateTask(taskList[currentTask].Name,
                    taskList[currentTask].StartTime);

                UpdateSummaryTable();

                return true;
            }
            else
            {
                SvnClient svnClient = new SvnClient();
                SvnInfoEventArgs Linfo;
                svnClient.GetInfo(vecLoc, out Linfo);
                SvnLockInfo lockInfo = Linfo.Lock;
                
                if (svnClient.Update(svnLoc) && 
                    ((lockInfo == null && svnClient.Lock(vecLoc, "")) 
                    || lockInfo.Owner == tBoxUname.Text))
                {
                    ProcessStartInfo info = new ProcessStartInfo();
                    info.FileName = airportExe;
                    info.WorkingDirectory = svnLoc + @"\tools\";
                    info.Arguments = wrkLoc;
                    info.LoadUserProfile = true;
                    info.UseShellExecute = true;
                    airportProcess = Process.Start(info);
                    airportProcess.EnableRaisingEvents = true;
                    airportProcess.Exited += AirportProcess_Exited;

                    dispTaskList = new List<Tasks>(taskList);

                    currentTask++;
                    taskList.Add(new Tasks());
                    taskListTbl.Items.Add(taskList[currentTask]);
                    taskList[currentTask].Name = taskName;
                    taskList[currentTask].StartTime = DateTime.Now;
                    taskListTbl.Items.Refresh();

                    db.CreateTask(taskList[currentTask].Name,
                        taskList[currentTask].StartTime);

                    UpdateSummaryTable();

                    return true;
                }
                else
                    MessageBox.Show("Failed to get lock.");
            }
            MessageBox.Show("Failed to update.");

            dispTaskList = new List<Tasks>(taskList);

            UpdateSummaryTable();

            return false;
        }

        private void AirportProcess_Exited(object sender, EventArgs e)
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                btnPause.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
            }));
        }

        private void StartMiscTask()
        {
            taskList.Add(new Tasks());
            currentTask = taskList.Count - 1;

            taskListTbl.Items.Add(taskList[currentTask]);
            taskList[currentTask].Name = "MISC";
            taskList[currentTask].StartTime = DateTime.Now;
            taskListTbl.Items.Refresh();

            db.CreateTask(taskList[currentTask].Name,
                taskList[currentTask].StartTime);

            dispTaskList = new List<Tasks>(taskList);

            UpdateSummaryTable();
        }

        private void UpdateCurrentTask()
        {
            taskList[currentTask].EndTime = DateTime.Now;
            taskList[currentTask].Hours = taskList[currentTask].EndTime - taskList[currentTask].StartTime.TimeOfDay;
            taskListTbl.Items.Refresh();

            db.EndTask(taskList[currentTask].Name,
                    taskList[currentTask].StartTime,
                    taskList[currentTask].EndTime,
                    taskList[currentTask].Hours);

            UpdateSummaryTable();
        }
        
        private void UpdateSummaryTable()
        {
            summaryTbl.Items.Clear();
            
            List<Tasks> summaryList = new List<Tasks>();

            IEnumerable<string> distinctTasks = dispTaskList.Select(x => x.Name).Distinct();

            foreach(string task in distinctTasks)
            {
                Tasks t = new Tasks();
                t.Name = task;
                t.Hours = DateTime.Now.Date;
                summaryList.Add(t);
                summaryTbl.Items.Add(t);
            }
            summaryList.Add(new Tasks());
            summaryTbl.Items.Add(summaryList[summaryList.Count-1]);
            summaryList[summaryList.Count - 1].Name = "Total";
            summaryList[summaryList.Count - 1].Hours = DateTime.Now.Date;

            foreach (Tasks task in dispTaskList)
            {
                foreach (Tasks summary in summaryList)
                {
                    if(task.Name.Trim().Equals(summary.Name.Trim()))
                    {
                        summary.Hours = summary.Hours.Add(task.Hours.TimeOfDay);                        
                    }
                }
                summaryList[summaryList.Count - 1].Hours = summaryList[summaryList.Count - 1].Hours.Add(task.Hours.TimeOfDay);
            }

            summaryTbl.Items.Refresh();
        }

        private void selectedDate_Changed(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            taskListTbl.Items.Clear();
            List<Tasks> Removables = new List<Tasks>();

            if (DateTime.Parse(selectedDate.SelectedDate.ToString()) != DateTime.Now.Date)
            {
                if (btnPlay.IsEnabled == true || btnPause.IsEnabled == true)
                {
                    prevPlayStatus = btnPlay.IsEnabled;
                    prevPauseStatus = btnPause.IsEnabled;
                }

                btnPlay.IsEnabled = false;
                btnPause.IsEnabled = false;

                dispTaskList.Clear();

                //Load previous entries
                dispTaskList = db.GetTasks(DateTime.Parse(selectedDate.SelectedDate.ToString()));
                foreach (Tasks t in dispTaskList)
                {
                    if (t.Hours < t.Hours.Date.Date.AddMinutes(5))
                    {
                        db.DeleteTask(
                            t.Name,
                            t.StartTime,
                            t.EndTime,
                            t.Hours);

                        Removables.Add(t);
                    }
                }

                foreach (Tasks t in Removables)
                {
                    dispTaskList.Remove(t);
                }

                foreach (Tasks t in dispTaskList)
                {
                    taskListTbl.Items.Add(t);
                }
                taskListTbl.Items.Refresh();
            }
            else
            {
                btnPlay.IsEnabled = prevPlayStatus;
                btnPause.IsEnabled = prevPauseStatus;
               

                if (taskList.Count == 0)
                {
                    //Load previous entries
                    taskList = db.GetTasks(DateTime.Parse(selectedDate.SelectedDate.ToString()));
                }

                for(int itr =0; itr < taskList.Count-1; itr++)
                {
                    if (taskList[itr].Hours < taskList[itr].Hours.Date.AddMinutes(5))
                    {
                        db.DeleteTask(
                            taskList[itr].Name,
                            taskList[itr].StartTime,
                            taskList[itr].EndTime,
                            taskList[itr].Hours);
                        Removables.Add(taskList[itr]);
                    }
                }

                foreach (Tasks t in Removables)
                {
                    taskList.Remove(t);
                }

                foreach (Tasks t in taskList)
                {
                    taskListTbl.Items.Add(t);
                }
                currentTask = taskList.Count - 1;
                taskListTbl.Items.Refresh();

                dispTaskList = new List<Tasks>(taskList);
            }

            UpdateSummaryTable();
        }

        private void LogIn(string username, string password, string market)
        {
            Mouse.OverrideCursor = Cursors.Wait;
            if (username == "")
            {
                MessageBox.Show(
                    "Please provide username",
                    "Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
            else if (password == "")
            {
                MessageBox.Show(
                    "Please provide password",
                    "Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
            else if (market == "")
            {
                MessageBox.Show(
                    "Please select market",
                    "Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
            else
            {
                if (JiraMan.LogIn(username, password))
                {
                    if (GetAssignedIssues())
                    {
                        this.Title += " - " + market;
                        LogInGrid.Visibility = Visibility.Hidden;
                        MainGrid.Visibility = Visibility.Visible;
                        AdjustWindow();
                    }
                }
                else
                {
                    MessageBox.Show(
                        "Unable to log in.\nPlease try again",
                        "Error",
                        MessageBoxButton.OK,
                        MessageBoxImage.Error);
                }
            }
            Mouse.OverrideCursor = null;
        }

        private void AdjustWindow()
        {
            this.Width = 360;
            this.Height = 675;
            double screenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
            double screenHeight = System.Windows.SystemParameters.PrimaryScreenHeight;
            double windowWidth = this.Width;
            double windowHeight = this.Height;
            this.Left = (screenWidth / 2) - (windowWidth / 2);
            this.Top = (screenHeight / 2) - (windowHeight / 2);
        }

        private bool GetAssignedIssues()
        {
            bool success = true;
            try
            {
                Mouse.OverrideCursor = Cursors.Wait;
                cboxTaskName.Items.Clear();
                foreach (Atlassian.Jira.Issue i in JiraMan.GetAssignedIssues(cBoxMarket.Text))
                {
                    string key = i.Key.Value;
                    key = key.Remove(0, 7);
                    cboxTaskName.Items.Add(key + " - " + JiraMan.GetFieldValue(i, "ICAO").ToUpper());
                }
                cboxTaskName.SelectedIndex = 0;
            }
            catch (Exception)
            {
                success = false;
                MessageBox.Show(
                    "You might have restricted access to " + cBoxMarket.Text + " in JIRA. Please contact your JIRA administrator.",
                    "Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
            finally
            {
                Mouse.OverrideCursor = null;
            }

            return success;
        }
        #endregion

        //Auto save function for database
        #region BackGround Update/Auto Save
        private BackgroundWorker autoUpdater;

        private void BackGroundUpdate()
        {
            autoUpdater = new BackgroundWorker();
            autoUpdater.DoWork += autoUpdater_DoWork;
            autoUpdater.RunWorkerAsync();
            Timer timer = new Timer(60000);
            timer.Elapsed += timer_Elapsed;
            timer.Start();
        }

        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (!autoUpdater.IsBusy)
                autoUpdater.RunWorkerAsync();
        }

        void autoUpdater_DoWork(object sender, DoWorkEventArgs e)
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                UpdateCurrentTask();

                Process[] pname = Process.GetProcessesByName("Airport");
                if (pname.Length > 0)
                {
                    this.Dispatcher.Invoke((Action)(() =>
                    {
                        if (btnPlay.IsEnabled && !IsActive)
                        {
                            tBarIcon.BalloonTipText = "Airport Tool is Running.\nClick here to open Task Monitor";
                            tBarIcon.ShowBalloonTip(10000);
                        }
                    }));
                }
            }));
        }
        #endregion
    }
}
