﻿using System;
using System.IO;
using System.Data;
using System.Data.SQLite;
using System.Windows;
using System.Collections.Generic;

namespace AMDB_Task_Monitor
{
    class Database
    {
        SQLiteConnection dbConnection;

        public Database(string dbLocation)
        {
            //Create database if it does not exist
            if (!File.Exists(dbLocation))
            {
                if (!Directory.Exists(Path.GetDirectoryName(dbLocation)))
                    Directory.CreateDirectory(Path.GetDirectoryName(dbLocation));
                SQLiteConnection.CreateFile(dbLocation);

                dbConnection = new SQLiteConnection("Data Source=" + dbLocation+ "; Version=3;");

                CreateTable();
            }
            else
            {
                dbConnection = new SQLiteConnection("Data Source=" + dbLocation + "; Version=3;");
            }
            
        }

        //Create Table
        private void CreateTable()
        {
            OpenDatabase();

            string sql = "CREATE TABLE AMDB_Tasks" + 
                " (Task VARCHAR(10), TaskDate DATE, StartTime TIME, EndTime TIME, Hours Time)";
            SQLiteCommand command = new SQLiteCommand(sql, dbConnection);
            command.ExecuteNonQuery();

            CloseDatabase();
        }

        //Create New Task
        public int CreateTask(string Name, DateTime StartTime)
        {
            int result;

            OpenDatabase();

            string sql = "INSERT INTO AMDB_Tasks (Task, TaskDate, StartTime, EndTime, Hours)" + 
                " VALUES (?, ?, ?, ?, ?)";

            SQLiteCommand command = new SQLiteCommand(sql, dbConnection);
            SQLiteParameter taskName = new SQLiteParameter();
            SQLiteParameter taskDate = new SQLiteParameter();
            SQLiteParameter startTime = new SQLiteParameter();
            SQLiteParameter endTime = new SQLiteParameter();
            SQLiteParameter numHours = new SQLiteParameter();

            taskName.Value = Name;
            taskDate.Value = StartTime.Date;
            startTime.Value = StartTime;
            endTime.Value = DateTime.Now;
            numHours.Value = DateTime.Now.Date;

            command.Parameters.Add(taskName);
            command.Parameters.Add(taskDate);
            command.Parameters.Add(startTime);
            command.Parameters.Add(endTime);
            command.Parameters.Add(numHours);

            result =  command.ExecuteNonQuery();

            CloseDatabase();

            return result;
        }

        //End a Task
        public int EndTask(string Name, DateTime StartTime, DateTime EndTime, DateTime Hours)
        {
            int result;

            OpenDatabase();

            string sql = "UPDATE AMDB_Tasks"+
                " SET EndTime=?, Hours=?" +
                " WHERE Task=? AND TaskDate=? And StartTime=?";

            SQLiteCommand command = new SQLiteCommand(sql, dbConnection);
            SQLiteParameter taskName = new SQLiteParameter();
            SQLiteParameter taskDate = new SQLiteParameter();
            SQLiteParameter startTime = new SQLiteParameter();
            SQLiteParameter endTime = new SQLiteParameter();
            SQLiteParameter numHours = new SQLiteParameter();
                    
            taskName.Value = Name;
            taskDate.Value = StartTime.Date;
            startTime.Value = StartTime;
            endTime.Value = EndTime;
            numHours.Value = Hours;
            
            command.Parameters.Add(endTime);
            command.Parameters.Add(numHours);
            command.Parameters.Add(taskName);
            command.Parameters.Add(taskDate);
            command.Parameters.Add(startTime);

            result = command.ExecuteNonQuery();

            CloseDatabase();

            return result;
        }

        //End a Task
        public int DeleteTask(string Name, DateTime StartTime, DateTime EndTime, DateTime Hours)
        {
            int result;

            OpenDatabase();

            string sql = "DELETE FROM AMDB_Tasks" +
                " WHERE EndTime=? AND Hours=? AND" +
                " Task=? AND TaskDate=? And StartTime=?;";

            SQLiteCommand command = new SQLiteCommand(sql, dbConnection);
            SQLiteParameter taskName = new SQLiteParameter();
            SQLiteParameter taskDate = new SQLiteParameter();
            SQLiteParameter startTime = new SQLiteParameter();
            SQLiteParameter endTime = new SQLiteParameter();
            SQLiteParameter numHours = new SQLiteParameter();

            taskName.Value = Name;
            taskDate.Value = StartTime.Date;
            startTime.Value = StartTime;
            endTime.Value = EndTime;
            numHours.Value = Hours;

            command.Parameters.Add(endTime);
            command.Parameters.Add(numHours);
            command.Parameters.Add(taskName);
            command.Parameters.Add(taskDate);
            command.Parameters.Add(startTime);

            result = command.ExecuteNonQuery();

            CloseDatabase();

            return result;
        }

        //Get Task List
        public List<Tasks> GetTasks(DateTime TaskDate)
        {
            List<Tasks> taskList = new List<Tasks>();

            OpenDatabase();

            string sql = "SELECT * FROM AMDB_Tasks" +
                " WHERE TaskDate=?";

            SQLiteCommand command = new SQLiteCommand(sql, dbConnection);
            SQLiteParameter taskDate = new SQLiteParameter();

            taskDate.Value = TaskDate.Date;

            command.Parameters.Add(taskDate);

            SQLiteDataReader reader = command.ExecuteReader();

            while(reader.Read())
            {
                Tasks task = new Tasks();
                task.Name = reader["Task"].ToString().ToUpper(); ;
                task.StartTime = DateTime.Parse(reader["StartTime"].ToString());
                task.EndTime = DateTime.Parse(reader["EndTime"].ToString());
                task.Hours = DateTime.Parse(reader["Hours"].ToString());
                taskList.Add(task);
            }

            CloseDatabase();

            return taskList;
        }

        //Open Database Connection
        private void OpenDatabase()
        {
            try
            {
                if (dbConnection.State == ConnectionState.Closed)
                {
                    dbConnection.Open();
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show("Unable to open database:\n" + exc.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        //Close Database Connection
        private void CloseDatabase()
        {
            try
            {
                if (dbConnection.State == ConnectionState.Open)
                {
                    dbConnection.Close();
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show("Unable to close database:\n" + exc.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
