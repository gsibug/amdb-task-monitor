﻿namespace AMDB_Task_Monitor
{
    class JIRA_Issue
    {
        public string KEY { get; set; }
        public string ICAO { get; set; }
        public string NAME { get; set; }
        public string SIZE { get; set; }
        public string PRIORITY { get; set; }
        public string STATUS { get; set; }
        public string EXTRACTOR { get; set; }
        public string PEER { get; set; }
        public string FORMAL { get; set; }
    }
}
