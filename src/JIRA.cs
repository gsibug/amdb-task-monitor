﻿using Atlassian.Jira;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMDB_Task_Monitor
{
    class JIRA
    {
        private string username;
        private Jira jira;

        public bool LogIn(string uName, string pWord)
        {
            jira = new Jira("https://dionysus.psware.com/jira/", uName, pWord);
            jira.MaxIssuesPerRequest = 50;

            try { jira.GetFilters(); }
            catch { return false; }

            username = uName;
            return true;
        }

        public List<Issue> GetAssignedIssues(string market)
        {
            string project = "";
            string jqlString = "";

            switch (market)
            {
                case "AIB":
                    project = "NPLAIB";
                    break;
                case "RCI":
                    project = "NPL";
                    break;
            }
            jqlString = "project = " + project + " AND status in (\"Formal Review\", \"Peer Review Defect Correction\"" +
                        ", \"Formal Review Defect Correction\", \"Peer Review\", Extraction, \"Ready For Extraction\", \"Peer Review Closeout\"" +
                        ", \"Formal Review Closeout\", \"Ready For Delivery\") AND assignee in (currentUser())";
            List<Issue> issues = jira.GetIssuesFromJql(jqlString).ToList();

            return issues;
        }

        public string GetFieldValue(Issue issue, string field)
        {
            try { return issue[field].Value; }
            catch { return ""; }

        }

    }
}
